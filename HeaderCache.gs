/**
*
* Author: Mikkel Holm Abrahamsen.
* Please give credit.
*/

function getHeaderCache() {
  var documentProperties = PropertiesService.getDocumentProperties();
  var headers = new Object();
  var i = 1;
  while (documentProperties.getProperty('HEADER' + i) !== null) {
    headers[documentProperties.getProperty('HEADER' + i)] = i;
    i++;
  }
  return headers;
}

function cacheHeaders() {
  var sheet = SpreadsheetApp.getActiveSheet();
  var columnCount = sheet.getLastColumn();
  var headerRange = sheet.getRange(1, 1, 1, columnCount);
  var headerRangeValues = headerRange.getValues();
  var documentProperties = PropertiesService.getDocumentProperties();
  var propertyDictionary = {};
  for (var i = 0; i < columnCount; i++) {
    propertyDictionary["HEADER" + (i + 1)] = headerRangeValues[0][i];
  }
  documentProperties.setProperties(propertyDictionary, true);
}

function clearHeaderCache() {
  var documentProperties = PropertiesService.getDocumentProperties();
  var i = 1;
  while (documentProperties.getProperty('HEADER' + i) !== null) {
    documentProperties.deleteProperty(documentProperties.getProperty('HEADER' + i));
    i++;
  }
}

function test_getHeaderCache() {
  var headers = getHeaderCache();
  Logger.log(headers);  
}