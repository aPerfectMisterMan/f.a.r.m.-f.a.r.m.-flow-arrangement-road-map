/**
*
* Author: Mikkel Holm Abrahamsen.
* Please give credit.
*/

var issueColors = {
  'New' : '#534141',
  'Needs info' : '#7A3610',
  'Accepted' : '#977D58',
  'Assigned' : '#5D5492',
  'Started' : '#5D7692',
  'Commented' : '#7A5D80',
  'Resolved' : '#648545',
  'Reviewed' : '#CC8545',
  'Closed' : '#60645A'
};

/**
* Used to dhndles edits to cells in the spreadsheet. 
*
* @param {event} the event object that is to be handled.
*/
function onEdit(e) {
  if (e) {
    var ss = e.source.getActiveSheet();
    var r = e.source.getActiveRange();
    
    // We only want to handle edits in the Issues Overview sheet.
    if (r.getRow() != 1) {
      switch (ss.getName()) {
        case "Issues Overview":
          handleIssuesOverviewEdit(e);
          break;
      }
    }
  }
}

function handleIssuesOverviewEdit(e) {
  var time = EucIT.getTimestamp_();
  var headers = EucIT.getHeaderCache();
  var ss = e.source.getActiveSheet();
  var r = e.source.getActiveRange();
  var headerRow = ss.getRange(1,1,1,ss.getMaxColumns());
  var status = ss.getRange(r.getRow(), headers['Status']).getValue();
  var editRow = ss.getRange(r.getRow(),1,1,ss.getMaxColumns());
  
  // Set Last modified
  var timestapColumn = EucIT.find('Last modified', headerRow);
  var timestampCell = editRow.getCell(1, timestapColumn.getColumn());
  timestampCell.setValue(time);
  
  // Set Created Time if blank.
  var createdTimeCell = ss.getRange(r.getRow(), headers['Created']);
  if (createdTimeCell.getValue() == '') {
    createdTimeCell.setValue(time);
  }
  
  // Set ID cell if blank.
  var IDCell = ss.getRange(r.getRow(), headers['ID']);  
  if (IDCell.getValue() == '') {
    IDCell.setValue(EucIT.getAndIncrementScriptProperty('NEXT_ISSUE_ID')); 
  }
  
  // Set Resolved Time if blank.
  var resolvedTimeCell = ss.getRange(r.getRow(), headers['Resolved']);
  if (status == 'Resolved' || status == 'Reviewed') {
    resolvedTimeCell.setValue(time);
  } else {
    resolvedTimeCell.setValue(''); 
  }
  
  // Set Resolved Time if blank.
  var AssignedTimeCell = ss.getRange(r.getRow(), headers['Assigned']);
  var AssignedToCell = ss.getRange(r.getRow(), headers['Assigned to']);
  if (status == 'Assigned' && AssignedToCell != null && AssignedToCell.getValue() != '') {
    AssignedTimeCell.setValue(time);
  }
  
  // Apply formatting to neutral so that when new rows are inserted, they are neutral too.
  applyFormatting('', editRow);      
  
  // Insert empty rows to the end so that new issues can be added.
  var freeRows = ss.getMaxRows() - editRow.getRow();
  
  if (freeRows < 10) {
    ss.insertRowsAfter(ss.getMaxRows(), 10 - freeRows + 1);
  }
  
  // Apply the proper formatting for the edited row.
  applyFormatting(status, editRow);
}

/*
function handleBugReportsEdit(e) {
  var time = EucIT.getTimestamp_();
  var headers = EucIT.getHeaderCache();
  var ss = e.source.getActiveSheet();
  var r = e.source.getActiveRange();
  var headerRow = ss.getRange(1,1,1,ss.getMaxColumns());
  
  // Set Last modified.
  var timestampCell = editRow.getCell(1, headers['Last modified']);
  timestampCell.setValue(time);
  
  // Set Created Time if blank.
  var createdTimeCell = ss.getRange(r.getRow(), headers['Created']);
  if (createdTimeCell.getValue() == '') {
    createdTimeCell.setValue(time);
  }
}
*/

/**
 * Applies the correct formatting to the cell
 */
function applyFormatting(status, row) {
  if (status in issueColors) {
    row.setBackgroundColor(issueColors[status]);
    row.setFontColor('#FFFFFF');
  } else {
    row.setBackgroundColor('#FFFFFF');
    row.setFontColor('#000000');
  }
  
  if (status == 'Closed') {
    row.setFontColor('#AAAAAA');
  }
}

/**
 * Changes the first issue's status to new (regardless of there was a status or not previously).
 */
function test_New_onEdit_IssuesOverview() {
  onEdit({
    user : "test_function",
    source : SpreadsheetApp.getActiveSpreadsheet(),
    range : SpreadsheetApp.getActiveSpreadsheet().getRange("B2"),
    value : "Status"
  });
}

function test_New_onEdit_BugReport() {
  onEdit({
    user : "test_function",
    source : SpreadsheetApp.getActiveSpreadsheet(),
    range : SpreadsheetApp.getActiveSpreadsheet().getRange("B2"),
    value : "Bug report test title"
  });
}