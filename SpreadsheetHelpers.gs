/**
*
* Author: Mikkel Holm Abrahamsen.
* Please give credit.
*/

function hLookup(value, range, row) {
  var cell = find(value, range[0]);
  if (cell === null) {
   return; 
  }
  return range[row-1][cell.getColumn()-1];
}


/**
 * Finds a value within a given range. 
 * @param value The value to find.
 * @param range The range to search in.
 * @return A range pointing to the first cell containing the value, 
 *     or null if not found.
 */
function find(value, range) {
  var data = range.getValues();
  for (var i = 0; i < data.length; i++) {
    for (var j = 0; j < data[i].length; j++) {
      if (data[i][j] == value) {
        return range.getCell(i + 1, j + 1);
      }
    }
  }
  return null;
}

function findCol(value, range) {
  return find(value, range).getColumn();  
}

function getTimestamp() {
    // Update timestamp.
    var d = new Date();
    var dateofDay = new Date(d.getTime());
    return Utilities.formatDate(dateofDay, "GMT+2", "MM/dd-yyyy HH:mmm:ss");  
}

function getAndIncrementScriptProperty(property) {
  // Change the unit type in the script property 'NEXT_ISSUE_ID'.
  var scriptProperties = PropertiesService.getScriptProperties();
  var nextIssueID = scriptProperties.getProperty(property);
  if (nextIssueID === null) {
  nextIssueID = 0; 
  } else {
  nextIssueID = parseInt(nextIssueID)+1; // Only changes local value, not stored value.
  }
    scriptProperties.setProperty(property, nextIssueID); // Updates stored value.
  return nextIssueID-1;
}

function overrideScriptProperty(inKey, inValue) {
  var scriptProperties = PropertiesService.getScriptProperties();
  scriptProperties.setProperty(inKey, inValue);
}

function setPropertyToThing() {
  overrideScriptProperty('NEXT_ISSUE_ID', 95);  
}